#!/usr/bin/env bash

################################
# Shows info about the weather (in Cincinnati) from accuweather.com
################################

full=""
short=""
color=""
status=0

URL='https://www.accuweather.com/pt/br/santo-ant%C3%B4nio-das-miss%C3%B5es/2309746/weather-forecast/2309746'
SITE="$(curl -s "$URL")"

weather="$(echo "$SITE" | awk -F\' '/acm_RecentLocationsCarousel\.push/{print $14 }'| head -1)"
temp="$(echo "$SITE" | awk -F\' '/acm_RecentLocationsCarousel\.push/{print $12"°" }'| head -1)"

if [[ $weather == *thunder* || $weather == *Thunder* ]]; then
    icon="SaM  "
else
    if [[ $weather == *rain* || $weather == *Rain* ]]; then
        icon="Sam  Chuva"
    else
        if [[ $weather == *cloud* || $weather == *Cloud* ]]; then
            icon="SaM  "
        else
            icon="SaM  "
        fi
    fi
fi

full="$icon $temp"
short="$temp"

echo $full
echo $short
exit $status
